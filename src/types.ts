export type Exchange = {
  code: Code

  buy(code: string, units: number): void

  sell(code: string, units: number): void

  getOrderBookTotalVolume(): Map<string, number>

  /** Returns dollar value of trading activity */
  getTradingCosts(): number
}

export type Code = 'ASX' | 'CXA'

export class InsufficientUnitsException extends Error {}

// We could throw a slightly more meaningful error message here with the failing code
export class InvalidCodeException extends Error {}

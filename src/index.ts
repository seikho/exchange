import * as minimist from 'minimist' // Light-weight command line argument parser
import { assertValid } from 'frisker' // A library I authored for light-weight object validation
import { getExchange } from './exchange'
import { getRandomCode, setCodeCost } from './exchange/code'

const MAX_UNITS = 100
const MAX_TRADES = 10000

const body = {
  exchange: ['ASX', 'CXA'],
} as const

/**
 * should buy and sell a random number of units of the stock codes (NAB,CBA,QAN),
 * a random number of times on the exchange specified by the command line arguments
 */
function trade() {
  const args = minimist(process.argv.slice(2))
  assertValid(body, args)

  const times = Math.trunc(Math.random() * MAX_TRADES)
  const exchange = getExchange(args.exchange)

  // Initialise code costs
  for (const [code] of exchange.getOrderBookTotalVolume().entries()) {
    setCodeCost(code, Math.trunc(Math.random() * 100))
  }

  for (let trade = 0; trade < times; trade++) {
    const code = getRandomCode()
    const action = Math.random() > 0.5 ? exchange.buy : exchange.sell
    const units = Math.trunc(Math.random() * MAX_UNITS) + 1

    try {
      action(code, units)
      console.log(
        'Code',
        code,
        'Units',
        units,
        'Costs',
        exchange.getTradingCosts().toLocaleString('en-AU', { style: 'currency', currency: 'AUD' })
      )
    } catch (ex: any) {
      console.warn(ex.constructor.name, { code, units })
    }
  }

  console.log('N =', times)
}

trade()

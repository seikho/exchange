import { expect } from 'chai'
import { InsufficientUnitsException, InvalidCodeException } from '../types'
import { setCodeCost } from './code'
import { createExchange } from './create'

const defaultBook = () => new Map<string, number>([['A', 100]])
const exch = (perTradeCost = 0) => {
  const exchange = createExchange({
    code: 'ASX',
    perTradeCost,
    orderBook: defaultBook(),
  })

  return exchange
}

// We won't test non-whole numbers and non-numbers

describe('exchange tests', () => {
  before(() => {
    setCodeCost('A', 10)
  })

  it('will allow buy when enough units', () => {
    const ex = exch()
    ex.buy('A', 100)
    expect(ex.getTradingCosts()).to.equal(10.0)
  })

  it('will disallow buy when insuffient units', () => {
    const fn = () => {
      exch().buy('A', 101)
    }

    expect(fn).to.throw(InsufficientUnitsException)
  })

  it('will disallow buying non-existent code', () => {
    const fn = () => {
      exch().buy('B', 50)
    }

    expect(fn).to.throw(InvalidCodeException)
  })

  it('will allow selling of units with known code', () => {
    const ex = exch()
    ex.sell('A', 10)
    expect(ex.getTradingCosts()).to.equal(1.0)
  })

  it('will disallowing selling with non-existent code', () => {
    const fn = () => {
      exch().sell('B', 10)
    }

    expect(fn).to.throw(InvalidCodeException)
  })

  it('will calculate the correct cumulative trading cost', () => {
    const ex = exch()
    ex.sell('A', 10)
    expect(ex.getTradingCosts()).to.equal(1.0)

    ex.sell('A', 10)
    expect(ex.getTradingCosts()).to.equal(2.0)

    ex.buy('A', 100)
    expect(ex.getTradingCosts()).to.equal(12.0)
  })

  it('will allow buying more than the initial book amount after selling', () => {
    const ex = exch()
    ex.sell('A', 10)

    const fn = () => {
      ex.buy('A', 110)
    }

    expect(fn).to.not.throw()
  })

  it('will disallow buying after volume is reduced by previous buy', () => {
    const ex = exch()
    ex.buy('A', 50)

    const fn = () => {
      ex.buy('A', 51)
    }

    expect(fn).to.throw(InsufficientUnitsException)
  })
})

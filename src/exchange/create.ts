import { Code, Exchange, InsufficientUnitsException, InvalidCodeException } from '../types'
import { getCodeCost } from './code'

type Trx = { code: string; units: number; cost: number }

type Opts = {
  code: Code
  perTradeCost: number

  // For use in tests
  orderBook?: Map<string, number>
}

// We should be disallowing non-whole numbers, but we'll forge ahead without

export function createExchange(opts: Opts): Exchange {
  const orderBook = opts.orderBook ?? getDefaultOrderBook()
  const transactions: Trx[] = []

  const getAvailable = (code: string) => {
    const available = orderBook.get(code)
    if (!available) throw new InvalidCodeException()

    return available
  }

  // Create the trading cost reducer function once to pay the function creation cost a single time
  const toTradingCost = toTradingCostReducer(opts.perTradeCost)

  // This could just as easily be a class
  const exchange: Exchange = {
    code: opts.code,
    buy: (code, units) => {
      const available = getAvailable(code)
      if (units > available) throw new InsufficientUnitsException()

      orderBook.set(code, units - available)
      transactions.push({ code, units, cost: getCodeCost(code) })
    },
    sell: (code, units) => {
      const available = getAvailable(code)
      orderBook.set(code, units + available)
      transactions.push({ code, units, cost: getCodeCost(code) })
    },
    getOrderBookTotalVolume: () => orderBook,
    getTradingCosts: () => transactions.reduce(toTradingCost, 0) * 0.01,
  }

  return exchange
}

function toTradingCostReducer(perTradeCost: number) {
  // I assume this is an "always positive" value for the amount of money being traded
  // I assume the "per trade cost" is also included
  const reduce = (prev: number, trx: Trx): number => {
    return trx.cost * trx.units + prev + perTradeCost
  }

  return reduce
}

function getDefaultOrderBook() {
  const book = new Map<string, number>([
    ['NAB', 100000],
    ['CBA', 100000],
    ['QAN', 100000],
  ])

  return book
}

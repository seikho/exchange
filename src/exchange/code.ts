import { InvalidCodeException } from '../types'

const codes: { [code: string]: number } = {}

export function getCodeCost(code: string): number {
  if (code in codes === false) {
    throw new InvalidCodeException()
  }

  return codes[code]
}

// get/set codes used for randomisation

export function setCodeCost(code: string, value: number) {
  codes[code] = value
}

export function getCodes(): string[] {
  return Object.keys(codes)
}

export function getRandomCode() {
  const codes = getCodes()
  const rand = Math.trunc(Math.random() * codes.length)
  return codes[rand]
}

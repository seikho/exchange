import { Code, Exchange, InvalidCodeException } from '../types'
import { createExchange } from './create'

export function getExchange(code: Code): Exchange {
  switch (code) {
    case 'ASX':
      return asx

    case 'CXA':
      return cxa
  }

  invalidExchange(code)
}

const asx = createExchange({
  code: 'ASX',
  perTradeCost: 5,
})

const cxa = createExchange({
  code: 'CXA',
  perTradeCost: 7,
})

// We use this "throw never" pattern to ensure the switch statement handles all intended cases
// Adding a new member to the Code union type (types.ts) will cause a compiler error
function invalidExchange(_n: never) {
  throw new InvalidCodeException()
}

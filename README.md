# Exchange

## Requirements

- NodeJS 12.x or above
- Yarn or NPM

## Getting Started

### Arguments

#### exchange

```
-exchange ASX | CXA
```

### NPM

```sh
> npm install
> npm run build
> npm run test

> npm start --exchange ASX
> npm start --exchange CXA
> npm start:cxa
> npm start:asx
```

### Yarn

```sh
> yarn
> yarn build
> yarn run test

> yarn start --exchange ASX
> yarn start --exchange CXA
> yarn start:cxa
> yarn start:asx
```
